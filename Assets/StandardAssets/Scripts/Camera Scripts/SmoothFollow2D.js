﻿ #pragma strict
 
 var target : Transform;
 var smoothTime = 0.3;
 var xOffest = 0.0f;
 var yOffest = 0.0f;
 
 
 private var thisTransform : Transform;
 private var velocity : Vector3;
 
 function Start()
 {
     thisTransform = transform;
 }
 
 function Update() 
 {
     thisTransform.position.x = Mathf.SmoothDamp( thisTransform.position.x, 
         target.position.x + xOffest, velocity.x, smoothTime);
     thisTransform.position.y = Mathf.SmoothDamp( thisTransform.position.y, 
         target.position.y + yOffest, velocity.y, smoothTime);
 }
 