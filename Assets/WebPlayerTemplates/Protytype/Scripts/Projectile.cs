﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	private GameObject target;

	// Use this for initialization
	void Start () {
		Destroy(this.gameObject, 3.5f);
	}
	
	// Update is called once per frame
	void Update () {
		LookAt2D(target.transform.position);
	}

	public void OnCollisionEnter2D(Collision2D collisionInfo){
		Destroy(this.gameObject, 0.1f);
	}

	public void OnTriggerExit2D(Collider2D collider){
		gameObject.GetComponent<SpriteRenderer>().color = Color.red;
	}

	public void SetTarget(GameObject target){
		this.target = target;
	}

	private void LookAt2D(Vector3 target){
		// LookAt 2D
		// get the angle
		Vector3 norTar = (target-transform.position).normalized;
		float angle = Mathf.Atan2(norTar.y,norTar.x)*Mathf.Rad2Deg;
		// rotate to angle
		Quaternion rotation = new Quaternion ();
		rotation.eulerAngles = new Vector3(0,0,angle);
		transform.rotation = rotation;
	}

}
