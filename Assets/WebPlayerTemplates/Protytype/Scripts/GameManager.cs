﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	//Singleton
	private static GameManager gameManager;
	public static GameManager GetGameManager(){
		if(gameManager==null){
			gameManager = GameObject.FindObjectOfType<GameManager>();
		}
		return gameManager;
	}
// --------------------------------------------------------------------

	private GridManager gridManager;
	public CharacterUser selectedPlayer; //{get;set;}

	public GameObject playerPrefab1;
	public GameObject playerPrefab2;
	public GameObject aiPrefab1;
	public GameObject aiPrefab2;
	public Rigidbody2D arrowPrefab;

	private Rigidbody arrowInstance;
	private Character arrowTarget;

	public GameObject fire;
	public GameObject earthWall;

	public GameObject defenseButtonP1;
	public GameObject defenseButtonP2;

//	private GameObject currentUi;


	public void CharacterPressed(CharacterUser character) {
		UpdateSelectedPlayer(character);

		if(selectedPlayer.charState == CharState.Moving){
//			currentUi = selectedPlayer.uI;
			gridManager.HighlightMoveRange(character.currentTile, character.moveRange);
			character.currentTile.HighlightOff();	//Because the player can't move to his current tile
		}
	}

	private void UpdateSelectedPlayer(CharacterUser newSelectedPlayer){
		ClearSelectedPlayerAndGUI ();
		
		selectedPlayer = newSelectedPlayer;
		selectedPlayer.renderer.material.color = Color.green;
		selectedPlayer.uI.SetActive(true);
	}
	
	public void ClearSelectedPlayerAndGUI (){
/*		if (currentUi != null) {
			currentUi.SetActive (false);
		}	*/
		if(selectedPlayer!=null){
			selectedPlayer.uI.SetActive(false);
			selectedPlayer.renderer.material.color = Color.white;
		}
	}

	public void AICharacterPressed(CharacterAI character) {

		if(selectedPlayer==null){
			return;
		}
		bool playerIsAttacking = (selectedPlayer.charState == CharState.Attacking);

		bool charInRange = character.currentTile.highlit;	//This might not work because TilePressed is also called
	
		if(playerIsAttacking && charInRange && selectedPlayer.currentAttack=="Arrow"){

			List<Tile> lineList = gridManager.GetLine(selectedPlayer.currentTile, character.currentTile);

			//TODO: implementazione con movimento proiettile quadrato per quadrato rispettando la linea. Ad ogni nuovo quadrato, si calcolano le proprietà ie. fuoco
			bool fireOnLine=false;
			foreach(Tile currentTile in lineList){
				if(currentTile.isOnFire()){
					fireOnLine = true;
				}
			}
			//TODO: impementazione dove il danno è un attributo del proiettile, ed anche essere infuocato. Viene passato il proiettile nella funzione Attack.
			selectedPlayer.Attack(character, fireOnLine, "arrow");
	//		character.Attack(selectedPlayer, fireOnLine, "arrow");
			gridManager.HighlightsOff();	
		}

		if(playerIsAttacking && charInRange && selectedPlayer.currentAttack=="Charge"){
			bool fireOnLine=false;
		
			for(int i=0; i<4; i++){
				Tile startTile = selectedPlayer.currentTile;
				switch(i){
					case 0: startTile.xIndex++; break;
					case 1:	startTile.xIndex--; break;
					case 2:	startTile.yIndex++; break;
					case 3: startTile.yIndex--; break;
				}

				List<Tile> lineList = gridManager.GetLine(startTile, character.currentTile);
				
				foreach(Tile currentTile in lineList){
					if(currentTile.isOnFire()){
						fireOnLine = true;
						Debug.Log("Fire Charge!");
						selectedPlayer.renderer.material.color = Color.red;
						goto End;

					}
				}
			}
		End:

			selectedPlayer.Attack (character, fireOnLine, "charge");
			gridManager.HighlightsOff();	
		}

	}

	public void TilePressed(Tile tile){
		CharacterUser selectedPlayer = GameManager.GetGameManager().selectedPlayer;

		if(selectedPlayer == null){
			return;
		}

		bool playerIsMoving = (selectedPlayer.charState == CharState.Moving);
		bool playerIsAttacking = (selectedPlayer.charState == CharState.Attacking);

		if(playerIsMoving && tile.highlit){
			selectedPlayer.Move(tile);
		}

		if(playerIsAttacking && tile.highlit && selectedPlayer.currentAttack=="Fire"){
			tile.SetOnFire(true);
			Vector3 firePosition = tile.transform.position;
			firePosition.z = firePosition.z-1;
			Instantiate(fire, firePosition, Quaternion.identity);
			TurnManager.GetTurnManager().NextTurn(selectedPlayer);
		}

		if(playerIsAttacking && tile.highlit && selectedPlayer.currentAttack=="Earth Wall"){
			//TODO: Divide the Earth Wall attack in 4 different directions, and compute where to place it on the tile based on the direction
			//This is for placing a North Earth Wall, this wall will protect a character standing north in respect to the wall
			float yOffset = (tile.renderer.bounds.size.y)/3;
			Vector3 earthWallPosition = tile.transform.position;
			earthWallPosition.y += yOffset; 

			Instantiate(earthWall, earthWallPosition, Quaternion.identity);
			TurnManager.GetTurnManager().NextTurn(selectedPlayer);
		}

		if(playerIsAttacking && tile.highlit && selectedPlayer.currentAttack=="Arrow"){
			return;
		}

		if(playerIsAttacking && tile.highlit && selectedPlayer.currentAttack=="Charge"){
			return;
		}

		gridManager.HighlightsOff();
	}

	//TODO: this method will need to have a second argument, Attack, with damage, range, texture, etc.
	public void CharacterAttacking(CharacterUser character){
		selectedPlayer = character;
		gridManager.HighlightAttackRange(character.currentTile, character.attackRange);
		character.currentTile.HighlightOff();	//Because the player can't attack his current tile
	}

	void Awake(){
		gameManager = GetGameManager();
	}

	void Start () {
		gridManager = GridManager.GetGridManager();
		CreatePlayers();
	}

	void Update () {
		
		if(arrowInstance!=null){
			float distanceFromTarget = Vector3.Distance(arrowInstance.position, arrowTarget.transform.position);
			if(distanceFromTarget >=0.1f){
				arrowInstance.transform.LookAt(arrowTarget.transform.position);
				arrowInstance.transform.position = Vector3.Lerp(arrowInstance.position, arrowTarget.transform.position, 2.5f * Time.deltaTime);
			} else {
				Destroy(arrowInstance.gameObject);
				arrowInstance = null;
				arrowTarget = null;
			}
		}
	}

	public void CreatePlayers() {

		int middleX = (int)(Mathf.Floor(gridManager.rowLength/2));
		int middleY = (int)(Mathf.Floor(gridManager.columnLength/2));

		Tile spawnTile = gridManager.GetTile(middleX, middleY);
		Vector3 middleTilePosition = spawnTile.transform.position;
	
		CharacterUser userPlayer1 = playerPrefab1.GetComponent<CharacterUser>();
		userPlayer1.Init(middleX, middleY);
		userPlayer1.transform.position = middleTilePosition;
		spawnTile.movable = false;
		userPlayer1.currentTile = spawnTile;
		spawnTile.charOnTile = userPlayer1;
//		userPlayer1.renderer.material.color = Color.green; // Linea usata con turni singoli

		spawnTile = gridManager.GetTile(middleX-1, middleY);
		middleTilePosition.x -= 1;

		CharacterUser userPlayer2 = playerPrefab2.GetComponent<CharacterUser>();
		userPlayer2.Init((middleX-1), middleY);
		userPlayer2.transform.position = middleTilePosition;
		spawnTile.movable = false;
		userPlayer2.currentTile = spawnTile;
		spawnTile.charOnTile = userPlayer2;

		spawnTile = gridManager.GetTile(0,0);
		Vector3 bottomLeftTilePosition = spawnTile.transform.position;

		CharacterAI aiPlayer1 = aiPrefab1.GetComponent<CharacterAI>();
		aiPlayer1.Init(0,0);
		aiPlayer1.transform.position = bottomLeftTilePosition;
		spawnTile.movable = false;
		aiPlayer1.currentTile = spawnTile;
		spawnTile.charOnTile = aiPlayer1;
		aiPlayer1.transform.renderer.material.color = Color.blue;

		spawnTile = gridManager.GetTile(1,0);
		bottomLeftTilePosition.x += 1;

		CharacterAI aiPlayer2 = aiPrefab2.GetComponent<CharacterAI>();
		aiPlayer2.Init(1,0);
		aiPlayer2.transform.position = bottomLeftTilePosition;
		spawnTile.movable = false;
		aiPlayer2.currentTile = spawnTile;
		spawnTile.charOnTile = aiPlayer2;
		aiPlayer2.transform.renderer.material.color = Color.blue;

	}
}
