﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour {

	private GameManager gameManager;

	public static int availableId=0;
	public int myId { get; private set; }
	public int xIndex { get; set; }
	public int yIndex { get; set; }
//	public bool highlit { get; private set; }
	public bool highlit;
	public bool movable { get; set; }
	public bool wallOnTile{ get; set; }

	public Character charOnTile;

	public Material standardMaterial;

	public Material onFireMaterial;
	public Material mouseOverMaterial;
	public Material moveHighlightMaterial;
	public Material attackHighlightMaterial;
	private Material currentMaterial;

	private bool onFire;

	public void SetOnFire(bool isOnFire){
		if(isOnFire==true){
			currentMaterial = onFireMaterial;
			onFire=true;
		} else {
			currentMaterial = standardMaterial;
			onFire=false;
		} 
	}

	public bool isOnFire(){
		return onFire;
	}

	void Awake() {
		movable = true;
		highlit = false;
		onFire = false;
	}

	void Start () {
		gameManager = GameManager.GetGameManager();
	}
	
	void Update () {
	
	}

	public void Init(int xIndex, int yIndex){
		this.myId = availableId;
		availableId++;
		this.xIndex = xIndex;
		this.yIndex = yIndex;
		this.currentMaterial = standardMaterial;
	}

	void OnMouseEnter() {
		this.transform.renderer.material = mouseOverMaterial;
	}

	void OnMouseExit() {
		this.transform.renderer.material = currentMaterial;
	}

	void OnMouseUpAsButton() {
		gameManager.TilePressed(this);
	}

	public void MoveHighlightOn(){
		if(movable) {
			this.transform.renderer.material = currentMaterial = moveHighlightMaterial ;
			highlit = true;
		}
	}

	public void AttackHighlightOn(){
		this.transform.renderer.material = currentMaterial = attackHighlightMaterial;
		highlit = true;
	}

	public void HighlightOff(){
		if(highlit){
			if(!onFire){
				this.transform.renderer.material = currentMaterial = standardMaterial;
			}else{
				this.transform.renderer.material = currentMaterial = onFireMaterial;
			}
			highlit = false;
		}
	}

	public override string ToString(){
		return xIndex+", "+yIndex;
	}

}
