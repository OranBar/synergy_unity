﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CharacterAI : Character {

	private List<Tile> tilesInMoveRange;
	private CharacterUser[] opponents;
	private CharacterUser target;

	public GameObject deathText;

	public float defenseTiming=1.2f;

	private bool okToAttack=false;

	void Start () {
		okToAttack=false;
	}
	
	internal override void Update () {
		base.Update();
		if(!move && okToAttack){
			Attack();
			okToAttack=false;
		}
	}

	void OnMouseUpAsButton() {
		GameManager.GetGameManager().AICharacterPressed(this);
	}

	private void PrintTilesInMoveRange(){
		foreach(Tile current in tilesInMoveRange){
			Debug.Log(current);
		}
	}

	public void FindOpponents(){
		GameObject[] temp = (GameObject.FindGameObjectsWithTag("Player")) ;
		opponents = new CharacterUser[temp.Length];
		for(int i=0; i<temp.Length; i++){
			opponents[i] = temp[i].GetComponent<CharacterUser>();
		}
	}

	public void ExecuteTurn(){
		if(TurnManager.GetTurnManager().turn == Turn.User){
			return;
		}
		tilesInMoveRange = new List<Tile>();
		ComputeMove (currentTile, moveRange);
//		PrintTilesInMoveRange();
		MoveToTileInRangeOfEnemy();
		new WaitForSeconds(2.0f);
		okToAttack=true;
	}

	private void MoveToTileInRangeOfEnemy(){
		FindOpponents();
		foreach(CharacterUser opponent in opponents){
			int x = xIndex, y = yIndex;
			int distance = Mathf.Abs(x - opponent.currentTile.xIndex) + Mathf.Abs(y-opponent.currentTile.yIndex);

			if(distance<=attackRange && CheckClearShot(currentTile, opponent)){	// If already in range, break
				Debug.Log("Standing still");
				target = opponent;
				return;
			}

			foreach(Tile possibleMoveTile in tilesInMoveRange ){

				if(possibleMoveTile.movable==false) continue;

				x = possibleMoveTile.xIndex;
				y = possibleMoveTile.yIndex;


				distance = Mathf.Abs(x - opponent.currentTile.xIndex) + Mathf.Abs(y - opponent.currentTile.yIndex);

				if(distance<=attackRange && CheckClearShot(possibleMoveTile, opponent)){	//If this possibleMoveTile is close enough for attacking, and nobody is in the way, move there
					target = opponent;
					Move (possibleMoveTile);
					return;
				}

			}
		}
		Debug.Log("Moving to last tile in tilesInMoveRange");
		target = null;
		Move(tilesInMoveRange[tilesInMoveRange.Count-1]);

	}

	private bool CheckClearShot(Tile tile, CharacterUser opponent){
		bool clearShot = true;
		List<Tile> tilesInAttackLine = GridManager.GetGridManager().GetLine(tile, opponent.currentTile);
		foreach(Tile currentTile in tilesInAttackLine){
			//The commented out condition allows the AI to stand still in its turn. It makes it seem less intelligent, even tho it's te other way around
			if(currentTile.charOnTile is CharacterAI /*&& currentTile.charOnTile!=this*/){	
				clearShot=false;
				break;
			}
			if(currentTile.wallOnTile){
				clearShot=false;
				break;
			}
		}
		return clearShot;
	}

	private void ComputeMove(Tile startTile, int range){
		if( !(tilesInMoveRange.Contains(startTile)) ){
			tilesInMoveRange.Add(startTile);
		}
		if(range==0){
			return;
		} else {
			List<Tile> adjacentTilesList = GridManager.GetGridManager().GetAdjacentTiles(startTile);
			foreach(Tile adjacentTile in adjacentTilesList){
				ComputeMove(adjacentTile, range-1);
			}
		}
	
	}

	//Public for testing only
	public void Attack(){
		if(target!=null && okToAttack){
			List<Tile> lineList = GridManager.GetGridManager().GetLine(this.currentTile, target.currentTile);
			
			//TODO: implementazione con movimento proiettile quadrato per quadrato rispettando la linea. Ad ogni nuovo quadrato, si calcolano le proprietà ie. fuoco
			bool fireOnLine=false;
			foreach(Tile currentTile in lineList){
				if(currentTile.isOnFire()){
					fireOnLine = true;
				}
			}
			//TODO: impementazione dove il danno è un attributo del proiettile, ed anche essere infuocato. Viene passato il proiettile nella funzione Attack.
			target.AllowDefense(this);
			StartCoroutine( allowUserDefense(fireOnLine) );
		
		}
	}

	IEnumerator allowUserDefense(bool fireOnLine){
		target.ActivateDefenseButton(true);
		yield return new WaitForSeconds(defenseTiming);
		this.Attack(target, fireOnLine, "arrow");
		target.ActivateDefenseButton(false);
	}

	protected override void Death(){
		StartCoroutine (ShowDeathText());
	}

	IEnumerator ShowDeathText(){
		deathText.SetActive(true);
		yield return new WaitForSeconds(3.0f);
		deathText.SetActive(false);
		this.gameObject.SetActive(false);
	}
}

	




