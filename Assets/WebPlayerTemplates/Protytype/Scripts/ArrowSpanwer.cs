﻿using UnityEngine;
using System.Collections;

public class ArrowSpanwer : MonoBehaviour {

	//Transform from where the arrows are shot

	public GameObject target;
	public Rigidbody2D freccia;

//	private Rigidbody2D frecciaInstance;
	private bool shoot;

	public int force=350;


	public void ShootTrue(){
		shoot = true;
	}

	void Start () {
		force=350;
		shoot=false;
	}

	void Update () {
		if(shoot){
			shoot=false;
			transform.LookAt(target.transform);
		//	If the object is a RigidBody2D, you better cast it to that type. It doesn't like being a GameObject.
			Rigidbody2D frecciaInstance;
			frecciaInstance = Instantiate(freccia, transform.position, Quaternion.identity) as Rigidbody2D;
			frecciaInstance.GetComponent<Projectile>().enabled = true; 
			frecciaInstance.GetComponent<Projectile>().SetTarget(target); 
			frecciaInstance.AddForce(transform.forward * force);
			Physics2D.IgnoreCollision(frecciaInstance.collider2D, this.gameObject.collider2D);
		}	
	}


}
