using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterUser : Character {

	public string currentAttack;

	public GameObject uI;

	// Use this for initialization
	void Start () {
		
	}
	
	internal override void Update () {
		base.Update();
	}

	void OnMouseUpAsButton() {
		if(TurnManager.GetTurnManager().turn == Turn.Ai){
			return;
		}
		
		GridManager.GetGridManager().HighlightsOff();
		charState = CharState.Moving;
		GameManager.GetGameManager().CharacterPressed(this);

	}

	public void FinishPlayerTurn(){
		TurnManager.GetTurnManager().FinishPlayerTurn();
	}

	public void HighligthAttack() {
		charState = CharState.Attacking;
		GameManager.GetGameManager().CharacterAttacking(this);
	}

	public void UseArrow(){
		if(TurnManager.GetTurnManager().turn != Turn.User){
			return;
		}
		GridManager.GetGridManager().HighlightsOff();
		currentAttack = "Arrow";
		HighligthAttack();
	}

	public void UseFire(){
		if(TurnManager.GetTurnManager().turn != Turn.User){
			return;
		}
		GridManager.GetGridManager().HighlightsOff();
		currentAttack = "Fire";
		HighligthAttack();
	}

	public void UseEarthWall(){
		if(TurnManager.GetTurnManager().turn != Turn.User){
			return;
		}
		GridManager.GetGridManager().HighlightsOff();
		currentAttack="Earth Wall";
		HighligthAttack();
	}

	public void UseCharge(){
		if(TurnManager.GetTurnManager().turn != Turn.User){
			return;
		}
		GridManager.GetGridManager().HighlightsOff();
		currentAttack="Charge";
		HighligthAttack();
		AdjacentTilesHighlightOff();

	}



	private void AdjacentTilesHighlightOff(){
		List<Tile> adjacentTiles = GridManager.GetGridManager().GetAdjacentTiles(currentTile);
		foreach(Tile current in adjacentTiles){
			current.HighlightOff();
		}
	}

}
